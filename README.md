# Kubernetes-101

Une petite conférence (1H environ) pour présenter kubernetes.

*public cible* : tous les développeurs qui ont à héberger leurs applications sur du Kubernetes, les ops en devenir, les curieux, etc.

## Présentation

Les slides sont disponibles : [ici](https://docs.google.com/presentation/d/121H_klCg364oJGIIcj23lezvZNDjgjVkeVc0NeAxuM8/edit?usp=sharing)

## Atelier

Les fichiers nécessaires sont dans le répertoire *ressoures*

### Installation d'un environnement

Nous pouvons créer des clusters kubernetes sur nos machines avec plusieurs outils différents, selon notre distribution ou nos envies.

- [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [k3s](https://k3s.io/)
- [Kind](https://kind.sigs.k8s.io/)

#### Installation avec Kind, pour ce labo

Je vais lancer un cluster avec Kind sur mon poste, je ne montre pas comment on installe un cluster, étant donné que c'est un métier, et qu'en général, une équipe très dédiée, ou une entreprise, le fait pour nous.

```bash
kind create cluster --config kind-config.yaml
```

Cette configuration permet de mapper les ports 80 et 443 sur notre machine.


### Déploiement de notre application (POD)

Une fois que vous avez votre cluster prêt, et que vous savez vous y connecter :

```bash
kubectl cluster-info
```

Alors, vous pouvez vous créer un namespace, afin de ranger vos affaires.

```bash
kubectl create namespace kub101
```

Switchez sur ce nouveau namespace

```bash
kubectl config set-context --current --namespace=kub101
```

> **Note** :
>
> N'ayez crainte, il existe des outils qui permettent de switcher de namespace bien plus facilement (kubens, par exemple)

Une fois dans votre nouveau namespace, créons un pod.

Pour créer un pod, nous pouvons soit utiliser la ligne de commande, ou uploader un fichier à l'api server, en nous servant de kubectl. Kubectl n'est en fait qu'un client http, connecté a l'API Server de notre cluster K8S. Finalement, oui, on pourrait administrer K8S avec un postman ;-)

```bash
kubectl run appli-front-pod --image=satare/appli:front --generator=run-pod/v1 --image-pull-policy=Always --labels='tier=frontend'
```

Ou, nous pouvons simplement uploader le fichier manifest "pod.yml" qui se trouve dans le dossier ressources. Je vous invite a l'ouvrir, et voir à quoi il ressemble.

```bash
kubectl apply -f ressources/pod.yml
```

Vérifions les pods actuellement déployés dans notre namespace

```bash
kubectl get pods
```

Nous avons notre pod, déployé. Nous aimerions maintenant accéder a notre application. Pour cela, nous devons passer par un service.

## Les services

Les services sont des objets kubernetes, qui servent de point d'entrée pour des applications. Les services exposent des ports, et redirigent le traffic vers les pods qui correspondent a un selecteur. Les pods n'exposent pas directement leurs ports sur les noeuds K8S.

La force des services, c'est qu'ils sont dynamiques : si votre application tourne sur 3 pods (scalabilité), alors le service enverra le traffic aux 3 pods, chacun leur tour. Si votre application passe soudainement sur 5 pods, alors les 5 recevront le traffic, sans action de votre part.

Il existe plusieurs types de services, qui permettent ou non d'exposer notre application à l'extérieur de notre cluster (une application peut être consommée uniquement par d'autres applications internes au cluster, par exemple).

Analysons le fichier ressources/service.yml. On y retrouve, dans la partie spec, le sélecteur, le port du pod à atteindre, le port à exposer sur le service etc.

Appliquons le service.

```bash
kubectl apply -f service.yml
```

> **Note** :
>
> Le Port-Forward
>
>Le port-forward est un des "tours de force" que réalise l'outil kubectl. Il permet de mapper un port de votre ordinateur à un port d'un objet dans Kubernetes. Le plus souvent, on applique le port-forward sur un service (mais on peut aussi bien le faire sur un pod).
>
>```bash
>kubectl port-forward service/frontend 5000:80
># Route le port 5000 de votre ordinateur, sur le port 80 du service 'frontend'
>```

Si on ouvre notre navigateur préféré sur l'adresse <http://localhost:5000>, on accède a notre application frontend.

Nous allons pouvoir surveiller l'état de notre application tout au long de ce TP.

### Accedons à notre application depuis notre poste

Etant donné que nous avons mappé le port 80 de notre machine sur le cluster, alors, nous pouvons accéder a notre application en ouvrant 'localhost' dans notre navigateur préféré.

Modifions notre service 'front' afin de

- le transformer en type "Nodeport"
- Exposer le service sur le port 30080 du noeud sur lequel il est hébergé

> **Note** :
>
> La magie de Kubernetes opère ici. Les plus perspicaces remarqueront une incohérence entre la configuration de notre cluster (kind-config.yaml) et ce que l'on vient de faire. En effet, les pods sont hébergés sur les worker. Or, nous avons mappé le port 80 de notre machine sur le port 30080 du noeud  "control-plane". Mais alors, comment est-ce que mon application peut répondre ? Et bien c'est simple, quand on enregistre un service de type "nodeport", on choisit un port (ou le cluster en choisi un pour nous) en tout cas, il ne peut pas y avoir 2 services avec le même nodeport dans un cluster. Ce nodeport étant déclaré, quelquesoit le noeud qui recoit le traffic sur ce port va rediriger le traffic sur le noeud qui héberge notre pod.

## Déployons un replicaset

Nous avons déployé notre premier pod, mais que ce passe-t-il si on le supprime, ou qu'il plante, ou que le serveur sur lequel il tourne tombe ? Le pod est détruit, et ne se relance pas. Notre application n'est plus accessible. C'est justement pour parer à cette éventualité qu'ont été créés les replicaset.

Un replicaset est un objet qui va manager des pods. Quand on déploie un replicaset, on déploie un objet qui récupère les pods qui correspondent à une règle de sélection (basée sur les labels des pods), et va compter ces pods. Si le nombre de pods est égal au nombre qu'on a déclaré dans notre replicaset, alors il ne fait rien. Si il y en a trop, il va en supprimer. Si il n'y en a pas assez, il va les créer, en se basant sur un template de pod qui est intégré au replicaset.

Vous pouvez consulter le fichier ressources/replicaset.yml pour vous faire une idée de ce que je viens de dire.

Identifiez :

- la partie qui contient le selecteur
- le nombre de pod demandé
- le template de pod à créer

> **Note** : Les labels
>  
> Les labels sont très importants dans Kubernetes. Il s'agit de couples "clé/valeur" qu'on ajoute a nos objets, ce qui va permettre de retrouver nos affaires, de les regrouper. Ils sont placés dans la partie "Metadata" de nos objets

### Appliquons le fichier replicaset

```bash
kubectl apply -f replicaset.yml
```

Que constate-t-on ? Le replicaset n'a créé que deux nouveaux pods, car le premier pod, que nous avons déployé précédemment, correspond a son selecteur. Ce premier pod, en revanche, est dorénavant géré par le réplicaset.

### Supprimons le pod de l'étape précédente

le pod est supprimé, mais il est recréé, grace au replicaset. Ce dernier s'est basé sur son template, c'est pource qu'il n'a pas le meme nom

```bash
NAMESPACE  NAME                  READY  REASON  AGE
kub101    ReplicaSet/frontend   -              3m10s
kub101    ├─Pod/frontend-f99ph  True           3m10s
kub101    ├─Pod/frontend-pljc8  True           3m10s
kub101    └─Pod/frontend-tf7t2  True           3m10s
```

Et maintenant que notre application est déployée, c'est tout ? Nous n'allons jamais la mettre a jour ? allons, ça n'est pas serieux ...

## Déployons un déploiement (lolilol)

Notre application fonctionne. Félicitations!
Essayons de modifier le replicaset pour changer la version de l'image, passons de satare/front-app:1 a satare/front-app:2. Modifions en conséquence le fichier ressources/replicaset.yml, et re-déployons le.

```bash
kubectl apply -f replicaset.yml
```

Que constate-t-on ? Rien. En effet, le réplicaset a une tâche précise : surveiller qu'il y ait le bon nombre de pod, en bonne santé, qui corresponde à son sélecteur. C'est le cas. Donc, il n'y a aucune raison de se servir du template, même si celui-ci ne correspond plus à ce qui est déployé. Si nous supprimons un pod, par contre, il va le recréer, avec la nouvelle version de l'image.

```bash
kubectl delete pod <monpod>

kubectl describe pod <monNouveauPod> 
# On constate que le champ Containers.Image est a jour, avec la nouvelle version.
```

On peut s'arrêter là et casser les pods a la main, mais vous avouerez que la valeur ajoutée est faible. Et puis, on fait comment, en cas de rollback ? on re-change la valeur dans le template, et on re-casse des pods ? Allons...

### Les déploiements

Les déploiements sont des objets qui managent des replicaset. Ils permettent la mise a jour de l'application, et le rollback à une version antérieure.

Je vous invite a supprimer le replicaset, afin de repartir sur une version propre.

```bash
kubectl delete replicaset frontend
# Note : vous constaterez que les pods créés par le réplicaset sont détruits si ce dernier est détruit. On peut éviter ce comportement, en ajoutant l'option --cascade=false
```

Inspectez le fichier ressources/deployment.yml. On y voit deux déploiements (on peut cumuler plusieurs fichiers yml en un, si on sépare les deux par des --- ). Concentrons nous sur l'un d'entre eux, dans la partie spec, on retrouve un selecteur, un template, un nombre de réplicas. Jusqu'ici, cela ressemble à un réplicaset. La partie "strategy" est particulière au déploiement, il s'agit ici de la manière dont on va gérer les mises a jour de notre application. Dans notre exemple, on demande d'effectuer un rolling-upgrade, avec au maximum 25% de nos pods indisponibles lors de la mise a jour.

Appliquez le fichier.

```bash
kubectl apply -f deployment.yml

#On constate qu'un déploiement a été créé, ainsi qu'un replicaset, et enfin un pod
kubectl get deployment,replicaset,pod
```

```bash
NAMESPACE  NAME                               READY  REASON  AGE
kub101    Deployment/frontend                -              34s
kub101    └─ReplicaSet/frontend-6764759458   -              34s
kub101      └─Pod/frontend-6764759458-8rv2f  True           34s
```

A présent, modifions le fichier deployment.yml pour déployer 3 réplicas, et appliquons le fichier.

```bash
NAMESPACE  NAME                               READY  REASON  AGE
kub101    Deployment/frontend                -              16m
kub101    └─ReplicaSet/frontend-6764759458   -              16m
kub101      ├─Pod/frontend-6764759458-7t4kw  True           12s
kub101      ├─Pod/frontend-6764759458-8rv2f  True           16m
kub101      └─Pod/frontend-6764759458-d82vz  True           12s
```

Modifions a nouveau le fichier, et changeons la version de l'image front, passons en version 2

```yaml
      - image: satare/front-app:2
```

appliquons le fichier

```bash
kubectl apply -f deployment.yml
```

On constate que kubernetes, via le déploiement, détruit et recrée un seul pod a la fois. Notre service est donc toujours accessible, et la mise a jour se fait en douceur.

### Rollback

Si on regarde les replicaset, on constate qu'un nouveau réplicaset a été créé, et l'ancien a été gardé en mémoire. Il est juste 'scalé' à 0

```bash
kubectl get replicaset

NAME                  DESIRED   CURRENT   READY   AGE
backend-6c95b8cd8b    3         3         3       23m
frontend-666b85f84c   3         3         3       65s
frontend-6764759458   0         0         0       23m
```

On peut donc, en cas de rollback, revenir en arrière, remonter l'ancien replicaset, et désactiver le nouveau.

```bash
kubectl rollout undo deployment frontend
```

De nouveaux pods ont été créés, mais avec l'ancien template. On est donc revenus sur la version 1 de notre application.

## Notes

### Les manifest d'objets

Les manifest sont des fichiers que l'on envoie a l'APIserver, afin qu'il les valide, et les enregistre en base. Le format préconisé est le format Yaml, mais on peut travailler en JSON. Voyons cela comme une Payload.
Les fichiers ont généralement 4 paragraphes : 

- Kind: le type d'objet qu'on va envoyer
- Version: sa version (api)
- Metadata: c'est la partie ou où va définir les annotations, les labels, le namespace, etc.
- Spec: la "partie utile" du fichier, où se trouve l'information essentielle de notre objet

## divers

CHANGER L IMAGE

les arbres de dépendances sont faits gravce au plugin tree

Note sur les difference entre les distrib de Kube